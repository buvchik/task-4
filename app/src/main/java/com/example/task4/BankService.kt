package com.example.task4

import retrofit2.Call
import retrofit2.http.GET

interface BankService {
@GET("?city=Гомель")
suspend fun  getBanomatList(): List<Bankomat>
}