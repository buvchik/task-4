package com.example.task4
import com.google.gson.annotations.SerializedName

data class Bankomat(
    @SerializedName("gps_x") val gps_x: String,
    @SerializedName("gps_y") val gps_y: String
)
