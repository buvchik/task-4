package com.example.task4

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.example.task4.databinding.ActivityMainBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        val URL = "https://belarusbank.by/api/atm/"
        mMap = googleMap

        val gomel = LatLng(52.4313, 30.9937)
        mMap.addMarker(MarkerOptions().position(gomel).title(getString(R.string.city)))
        mMap.moveCamera(CameraUpdateFactory.newLatLng(gomel))
//        mMap.setMinZoomPreference(10f)

        val retrofit = Retrofit.Builder()
            .baseUrl(URL)
            .addConverterFactory(GsonConverterFactory
            .create())
            .build()
        val bankService = retrofit.create(BankService::class.java)

        CoroutineScope(Dispatchers.IO).launch {
            var list = bankService.getBanomatList()
            withContext(Dispatchers.Main) {
                list.forEach {
                    mMap.addMarker(
                        MarkerOptions().position(
                            LatLng(
                                it.gps_x.toDouble(), it.gps_y.toDouble()
                            )
                        )
                    )
                }
            }
        }
    }
}